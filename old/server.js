var express = require('express');
var routes = require('./routes/index');
var app = express();

// import renderer
app.set('view engine', 'jade');

// set routes
app.get('/', routes.index);

// set static dir
app.use(express.static(__dirname + '/public'));

// initiate server
var server = app.listen(9999, function () {
  var host = server.address().address
  var port = server.address().port
  console.log('Example app listening at http://%s:%s', host, port)
})
