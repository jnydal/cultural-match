       angular.module('app', ['services'])
          .config(['$routeProvider', function($routeProvider) {
            $routeProvider.
              when('/list', { templateUrl: 'partials/list.html', controller: 
PollListCtrl }).
              when('/get/:id', { templateUrl: 'partials/item.html', controller: 
PollItemCtrl }).
              when('/new', { templateUrl: 'partials/new.html', controller: 
PollNewCtrl }).
              otherwise({ redirectTo: '/list' });
          }]);
