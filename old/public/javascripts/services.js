       angular.module('services', ['ngResource']).
          factory('Profile', function($resource) {
            return $resource('get/:id', {}, {
              query: { method: 'GET', params: { id: 'get' }, isArray: true }
            })
          });
