var hofstedeFileUtil = require('../utils/hofstedeFileUtil');
var db = require('../utils/db');

/*
 * GET landing page.
 */
exports.index = function(req, res){
	res.render('index');
};

/*
 * switch for static pages like about and matcher
 */
exports.partial = function (req, res) {
  var name = req.params.name;
  res.render('partials/' + name);
};

/*
 * request handler for match request postings
 */
exports.calculate = function(req, res) {
	var weightedAnswers = db.getWeightedAnswers(req);
	var result = db.getCountry(weightedAnswers);
    var countryKey = hofstedeFileUtil.getCountry(result);
    res.redirect('result/' + countryKey);
}

/*
 * handles presentation of result view
 */
exports.result = function(req, res) {
	var country = hofstedeFileUtil.getCountryFromKey(req.params.countryKey);
	if (!country) {
		res.status(404).send('Not found');
	}
	res.render('partials/result', {
		country: country
	});
}
