/*
 * Question data for app
 */

exports.questions = function (req, res) {
  var struct = {
    "pd_questions": [
        {
            "id": "pd_q1",
            "category": "Power elites",
            "text": "How do you view power elites?",
            "answer_l": {
                "text": "Use of power should be legitimate and is subject to criteria of good and evil"
            },
            "answer_h": {
                "text": "Power is a basic fact of society antedating good or evil: its legitimacy is irrelevant"
            }
        },
        {
            "id": "pd_q2",
            "category": "Parenting",
            "text": "What is your opinion on parenting?",
            "answer_l": {
                "text": "Parents should treat children as equals"
            },
            "answer_h": {
                "text": "Parents should teach children obdience"
            }
        },
        {
            "id": "pd_q3",
            "category": "Elders",
            "text": "How do you feel towards people which is older than you?",
            "answer_l": {
                "text": "Older people are neither respected nor feared"
            },
            "answer_h": {
                "text": "Older people are both respected and feared"
            }
        },
        {
            "id": "pd_q4",
            "category": "School",
            "text": "What feels most right for you?",
            "answer_l": {
                "text": "Adapting teaching to student needs is the best"
            },
            "answer_h": {
                "text": "The student must respect and strive to understand the teacher"
            }
        },
        {
            "id": "pd_q5",
            "category": "Class differences",
            "text": "What feels most right for you?",
            "answer_l": {
                "text": "Hierarchy means inequality of roles, established for convenience"
            },
            "answer_h": {
                "text": "Hierarchy means existential inequality"
            }
        },
        {
            "id": "pd_q6",
            "category": "At work",
            "text": "Whats your stance regarding boss-employee relationships at work?",
            "answer_l": {
                "text": "Subordinates expect to be consulted"
            },
            "answer_h": {
                "text": "Subordinates expect to be told what to do"
            }
        },
        {
            "id": "pd_q7",
            "category": "Governments",
            "text": "Whats your stance regarding elections and governance of society?",
            "answer_l": {
                "text": "Pluralist governments based on majority vote and changed peacefully"
            },
            "answer_h": {
                "text": "Autocratic governments based on co-optation and changed by revolution"
            }
        },
        {
            "id": "pd_q8",
            "category": "Corruption",
            "text": "Whats your stance regarding helping close friends and family?",
            "answer_l": {
                "text": "Helping friends and family trough public job positions is never ok"
            },
            "answer_h": {
                "text": "Everybody is stealing somehow"
            }
        },
        {
            "id": "pd_q9",
            "category": "Income",
            "text": "Whats your stance regarding income?",
            "answer_l": {
                "text": "I\"m inclined towards ideas of even pay in society"
            },
            "answer_h": {
                "text": "Some people deserver higher pay than other"
            }
        },
        {
            "id": "pd_q10",
            "category": "Religion",
            "text": "Whats your stance regarding religion?",
            "answer_l": {
                "text": "Religion is about equity for all humans"
            },
            "answer_h": {
                "text": "Religious leaders are closer to God"
            }
        }
    ],
    "ua_questions": [
        {
            "id": "ua_q1",
            "category": "Uncertainity",
            "text": "How do you view life and uncertainity?",
            "answer_l": {
                "text": "I accept the uncertainity unherent in life, and takes each day as it comes"
            },
            "answer_h": {
                "text": "The uncertainity is felt like a threat which must be fought"
            }
        },
        {
            "id": "ua_q2",
            "category": "Uncertainity",
            "text": "How do you usually feel as an adult?",
            "answer_l": {
                "text": "At ease with self-control"
            },
            "answer_h": {
                "text": "Anxious, emotional and feeling stressed"
            }
        },
        {
            "id": "ua_q3",
            "category": "Wellbeeing",
            "text": "How are you?",
            "answer_l": {
                "text": "I feel like having charge of my life"
            },
            "answer_h": {
                "text": "I am not in control of my own life and wellbeeing"
            }
        },
        {
            "id": "ua_q4",
            "category": "Society",
            "text": "How do you preceive people which is different?",
            "answer_l": {
                "text": "Deviant persons and ideas are inspiring"
            },
            "answer_h": {
                "text": "Strange people are no good"
            }
        },
        {
            "id": "ua_q5",
            "category": "Order",
            "text": "What feels most right for you?",
            "answer_l": {
                "text": "I value presonal freedom, I have no need to live after dictated guidelines"
            },
            "answer_h": {
                "text": "I prefer clairity and structure in my life"
            }
        },
        {
            "id": "ua_q6",
            "category": "School",
            "text": "What do you think about teachers which says \"I don't know\"?",
            "answer_l": {
                "text": "Teachers may say \"I dont't know\", as its better than teaching wrongly"
            },
            "answer_h": {
                "text": "Teachers are suppose to have all answers no matter what"
            }
        },
        {
            "id": "ua_q7",
            "category": "Jobs",
            "text": "What do you think about chaning jobs?",
            "answer_l": {
                "text": "Changing jobs can give better benefits"
            },
            "answer_h": {
                "text": "The most important is to have a stable job"
            }
        },
        {
            "id": "ua_q8",
            "category": "Politics",
            "text": "How do you perceive authorities?",
            "answer_l": {
                "text": "As a common citizen I feel that authorities respect us"
            },
            "answer_h": {
                "text": "As a common citizen I feel not valued by authorities"
            }
        },
        {
            "id": "ua_q9",
            "category": "Religion",
            "text": "Are there absolute facts and truths in religion, philosophy and science?",
            "answer_l": {
                "text": "empirical data is the closest truths we have, and can provide probabilities of truths"
            },
            "answer_h": {
                "text": "science is constructed/business, and cannot be compared to the truth/God"
            }
        }
    ],
    "ci_questions": [
        {
            "id": "ci_q1",
            "category": "Society",
            "text": "Do you consider yourself to have personal loyality to your nation?",
            "answer_l": {
                "text": "I live for myself and my closest circle"
            },
            "answer_h": {
                "text": "The state owe my loyality"
            }
        },
        {
            "id": "ci_q2",
            "category": "Society",
            "text": "Do you believe in standing up for fellow humans in a community?",
            "answer_l": {
                "text": "Everyone is supposed to take care of him or herself and his or hers immediate family only"
            },
            "answer_h": {
                "text": "People are born into extended families or clans which protect them in exchange of loyalty"
            }
        },
        {
            "id": "ci_q3",
            "category": "Privacy",
            "text": "How do you view privacy?",
            "answer_l": {
                "text": "Everybody has the right of privacy"
            },
            "answer_h": {
                "text": "We are all fellow citizens"
            }
        },
        {
            "id": "ci_q4",
            "category": "Opinions",
            "text": "How do you view \"freak\" opinions?",
            "answer_l": {
                "text": "Speaking one\"s mind is healthy"
            },
            "answer_h": {
                "text": "Harmony should always be maintained"
            }
        },
        {
            "id": "ci_q5",
            "category": "View on others",
            "text": "How do you view other\"s opinions?",
            "answer_l": {
                "text": "Òthers are independent humans with own opinions"
            },
            "answer_h": {
                "text": "Other people\"s opinons are just the voice of their own community"
            }
        },
        {
            "id": "ci_q6",
            "category": "Opinions",
            "text": "What\"s your view regarding having opinions and beeing indifferent?",
            "answer_l": {
                "text": "Personal opinion is expected: one person one vote"
            },
            "answer_h": {
                "text": "Opinions and votes are predetermined by in-group/community"
            }
        },
        {
            "id": "ci_q7",
            "category": "Informal rules",
            "text": "How do you feel when breaking informal rules?",
            "answer_l": {
                "text": "Transgression of norms leads to guilt feelings"
            },
            "answer_h": {
                "text": "Transgression of norms leads to shame feelings"
            }
        },
        {
            "id": "ci_q8",
            "category": "Wording",
            "text": "How often do you use the word \"I\"?",
            "answer_l": {
                "text": "Using the word \"I\" is unavoidable"
            },
            "answer_h": {
                "text": "It\"s can often be seen as inpolite to use the word I, so its avoided"
            }
        },
        {
            "id": "ci_q9",
            "category": "Education",
            "text": "Have do you view the purpose of education?",
            "answer_l": {
                "text": "Purpose of educatoin is to learn how to learn"
            },
            "answer_h": {
                "text": "Purpose of education is learning how to do things"
            }
        },
        {
            "id": "ci_q9",
            "category": "Relationships vs. tasks",
            "text": "Have do you view the the importance of tasks?",
            "answer_l": {
                "text": "Tasks prevails over relationships"
            },
            "answer_h": {
                "text": "Relationships prevail over tasks"
            }
        }
    ],
    "mas_questions": [
        {
            "id": "mas_q1",
            "category": "Gender roles",
            "text": "What is your opinion regarding gender roles?",
            "answer_l": {
                "text": "Both men and women should be allowed to express themselves as they feel"
            },
            "answer_h": {
                "text": "Men should behave as men, and women as women"
            }
        },
        {
            "id": "mas_q2",
            "category": "Gender roles",
            "text": "With regards of raising children",
            "answer_l": {
                "text": "Men and women should be modest and caring"
            },
            "answer_h": {
                "text": "Men should be, and woment may be assertive and ambitious"
            }
        },
        {
            "id": "mas_q3",
            "category": "Life and work",
            "text": "How do you see the ideal work-life balance?",
            "answer_l": {
                "text": "There should be a balance between family and work"
            },
            "answer_h": {
                "text": "Work prevails over family"
            }
        },
        {
            "id": "mas_q4",
            "category": "Ideals",
            "text": "On a psychological level, do you view recklessnes or carelessness as strength?",
            "answer_l": {
                "text": "Weak people is a result of psychopaths nurturing themselves on them"
            },
            "answer_h": {
                "text": "We live in a competitive world, we have winners and loosers"
            }
        },
        {
            "id": "mas_q5",
            "category": "Gender roles",
            "text": "What is appropiate regarding expression of emotions vs. facts?",
            "answer_l": {
                "text": "Both fathers and mothers deal with facts and feelings"
            },
            "answer_h": {
                "text": "Fathers deals with facts, mothers with feelings"
            }
        },
        {
            "id": "mas_q6",
            "category": "Regarding children",
            "text": "Do you think its ok that your childs are fighting?",
            "answer_l": {
                "text": "Both boys and girls may cry but neither should fight"
            },
            "answer_h": {
                "text": "Girls cry, boys don\"t; boys should fight back, girls shouldn\"t fight."
            }
        },
        {
            "id": "mas_q7",
            "category": "Regarding children",
            "text": "",
            "answer_l": {
                "text": "Mothers should decide on number of children"
            },
            "answer_h": {
                "text": "Fathers decide on family size"
            }
        },
        {
            "id": "mas_q8",
            "category": "Politics",
            "text": "Whta do you think about women in political positions?",
            "answer_l": {
                "text": "There is no reason women should not be in political positions"
            },
            "answer_h": {
                "text": "Women has nothing to do as politicians"
            }
        },
        {
            "id": "mas_q9",
            "category": "Religion",
            "text": "Is it for people or for God?",
            "answer_l": {
                "text": "Religion should be for fellow human beeings"
            },
            "answer_h": {
                "text": "Religion is to satisfy God"
            }
        },
        {
            "id": "mas_q9",
            "category": "Sexuality",
            "text": "",
            "answer_l": {
                "text": "Sex is a natural way of relating between humans"
            },
            "answer_h": {
                "text": "Sex is about roles and performing"
            }
        }
    ],
    "lto_questions": [
        {
            "id": "lto_q1",
            "category": "Time orientation",
            "text": "Do you live in the moment?",
            "answer_l": {
                "text": "The most important events occured in the past or take place now"
            },
            "answer_h": {
                "text": "The most important events will occur in the future"
            }
        },
        {
            "id": "lto_q2",
            "category": "Archetype of a good person",
            "text": "",
            "answer_l": {
                "text": "Personal steadyness amd stability: a good person is always the same"
            },
            "answer_h": {
                "text": "A good person adapts to the circumstances"
            }
        },
        {
            "id": "lto_q3",
            "category": "Good and evil",
            "text": "",
            "answer_l": {
                "text": "There are universal guidelines about what is good and evil"
            },
            "answer_h": {
                "text": "Good and evil depends upon the circumstances"
            }
        },
        {
            "id": "lto_q4",
            "category": "Traditions",
            "text": "",
            "answer_l": {
                "text": "Traditions are holy"
            },
            "answer_h": {
                "text": "Traditions are adaptable to changed circumstances"
            }
        },
        {
            "id": "lto_q5",
            "category": "Family life",
            "text": "",
            "answer_l": {
                "text": "Family life is guided by imperatives like gender roles, culture and traditions"
            },
            "answer_h": {
                "text": "Family life is guided by shared tasks"
            }
        },
        {
            "id": "lto_q6",
            "category": "Patriotism",
            "text": "",
            "answer_l": {
                "text": "I am or want to be proud of my country"
            },
            "answer_h": {
                "text": "I want to learn from other counties"
            }
        },
        {
            "id": "lto_q7",
            "category": "Hospitality",
            "text": "",
            "answer_l": {
                "text": "Service to others is important"
            },
            "answer_h": {
                "text": "Thrift and perseverance are important goals"
            }
        },
        {
            "id": "lto_q8",
            "category": "Spending tendency",
            "text": "How do you spend your money?",
            "answer_l": {
                "text": "Social spending and consumption"
            },
            "answer_h": {
                "text": "I try to save and invest for the future"
            }
        },
        {
            "id": "lto_q9",
            "category": "Success",
            "text": "Most hard work or luck?",
            "answer_l": {
                "text": "Success is mostly about luck"
            },
            "answer_h": {
                "text": "Success is mostly attributed to effort"
            }
        }
    ],
    "ir_questions": [
        {
            "id": "ir_q1",
            "category": "Happiness",
            "text": "Do you feel free in your life?",
            "answer_l": {
                "text": "I feel happy"
            },
            "answer_h": {
                "text": "Happyness is irrelevant for me"
            }
        },
        {
            "id": "ir_q2",
            "category": "Freedom",
            "text": "Do you feel in charge of your life?",
            "answer_l": {
                "text": "I have a perception of personal life control"
            },
            "answer_h": {
                "text": "What happens with me is out of my control"
            }
        },
        {
            "id": "ir_q3",
            "category": "Freedom of speech",
            "text": "",
            "answer_l": {
                "text": "Everybody have the right to get heard"
            },
            "answer_h": {
                "text": "Some people should not be allowed to express themselves in public"
            }
        },
        {
            "id": "ir_q4",
            "category": "Life comfort",
            "text": "",
            "answer_l": {
                "text": "Leisure is important in my life"
            },
            "answer_h": {
                "text": "Leisure is not neccesarry for me"
            }
        },
        {
            "id": "ir_q5",
            "category": "Happiness",
            "text": "",
            "answer_l": {
                "text": "Happy moments in my life is always dear to me"
            },
            "answer_h": {
                "text": "I have no time to reflect on the past in my chaotic life situation"
            }
        },
        {
            "id": "ir_q6",
            "category": "Births",
            "text": "",
            "answer_l": {
                "text": "Educated people should have many children"
            },
            "answer_h": {
                "text": "Educated people destroy their lives by getting many children"
            }
        },
        {
            "id": "ir_q7",
            "category": "Sports",
            "text": "",
            "answer_l": {
                "text": "Sports make life interesting"
            },
            "answer_h": {
                "text": "Sports is not neccesarry, and is almost annoying"
            }
        },
        {
            "id": "ir_q8",
            "category": "Food",
            "text": "",
            "answer_l": {
                "text": "I\"m often hungry, and i eat until i\"m full"
            },
            "answer_h": {
                "text": "Eating for me is more about taste experience"
            }
        },
        {
            "id": "ir_q9",
            "category": "Sexuality",
            "text": "",
            "answer_l": {
                "text": "People should feel sexually free"
            },
            "answer_h": {
                "text": "People should respect traditional sexual norms"
            }
        },
        {
            "id": "ir_q10",
            "category": "Society",
            "text": "",
            "answer_l": {
                "text": "People should feel free in the society"
            },
            "answer_h": {
                "text": "More police is better"
            }
        }
    ]
  };
  
  /** 
   * convert question structure so its category oriented 
   * 
   */
  function getCategoryView(struct) {
	  
	  var res = {};
	  for (dimensionkey in struct) {
		  var dimension = struct[dimensionkey];
		  for (key in dimension) {
			  if (typeof res[dimension[key].category] === "undefined") {
				  res[dimension[key].category] = [];
			  }
			  var keyname = dimension[key].category;
			  //delete dimension[key].category;
			  res[keyname].push(dimension[key]);
		  }		  
	  }
	  return res;
  };
  
  res.json({
	  "qcategories": getCategoryView(struct)
  });
};