/* depricated */

var synaptic = require('synaptic'),
	hofstedeFileUtil = require('./hofstedeFileUtil');

exports.COGNITION = undefined;

// train if not trained
(function() {
	if (typeof exports.COGNITION === "undefined") {	 
		exports.COGNITION = new synaptic.Architect.Hopfield(35);
	    hofstedeFileUtil.getTrainingSet(function(trainingSet) {
	    	exports.COGNITION.learn(trainingSet);
	    });
	}
})();

exports.getCountry = function(result) {
	var weight = 0;
	var max;
	for (country in result) {
		if (result[country] > weight) {
			weight = result[country];
			max = country
		}
	}
	return max;
};

function getEmptryAnswerPoints() {
	return {
		p: {
			l: 0,
			h: 0
		},
		u: {
			l: 0,
			h: 0
		},
		i: {
			l: 0,
			h: 0
		},
		m: {
			l: 0,
			h: 0
		},
		l: {
			l: 0,
			h: 0
		}
	}
}

function getAnswerWeight(dim, answerPoints) {
	var l = answerPoints[dim].l;
	var h = answerPoints[dim].h;
	var tot = l + h;
	return (l / tot)
}

function getCountryFromHopfieldOutput(output) {
	var binary = "";
	for (var i = 0; i<output.length; i++) {
		binary += output[i];
	}
	return exports.HOPFIELD_MAP[parseInt(binary,2).toString(10)];
}

exports.getNeuralAnswers = function (req) {
	var answerMap = req.body;
	var answerPoints = getEmptryAnswerPoints();
	var charAt0, answer, response;
	for (questionKey in answerMap) {
		charAt0 = questionKey.charAt(0);
		answer = answerMap[questionKey];
		if (typeof answer === "string") {
			response = answer.charAt(7);
		} else {
			// todo
		}
		if (answerPoints[charAt0]) {
			answerPoints[charAt0][response]++;
		}	
	}
	var result = {
		pd: getAnswerWeight('p', answerPoints),
		ua: getAnswerWeight('u', answerPoints),
		mas: getAnswerWeight('m', answerPoints),
		ic: getAnswerWeight('i', answerPoints),
		lto: getAnswerWeight('l', answerPoints)
	}
	return hofstedeFileUtil.toHopfieldInput(result);
};

exports.sim = function (neuralAnswers) {
	var netOutput = exports.COGNITION.feed(neuralAnswers);
	return getCountryFromHopfieldOutput(netOutput);
};
