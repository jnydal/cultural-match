var fs = require('fs');
var parse = require('csv-parse');
var async = require('async');

var TRAINING_SET = [];

var FILENAME = "hofstede2.csv";
var SCALE_MAX = 100.0;
var COUNTRYNAME_POS = 0;
var COUNTRYPD_POS = 1;
var COUNTRYUA_POS = 2;
var COUNTRYIC_POS = 3;
var COUNTRYMAS_POS = 4;
var COUNTRYLTO_POS = 5;

var countries = [];

var COUNTRY_MAP = {};

function getWeight(value) {
	return value / SCALE_MAX;
}

function populateOutput(countryName, output) {
	for (var i = 0; i<countries.length; i++) {
		if (countryName === countries[i]) {
			output[countries[i]] = 1;
		} else {
			output[countries[i]] = 0;
		}
	}
}

function put(countryProfile, set) {
	if (countryProfile[COUNTRYNAME_POS] !== "") {
		var entry = {
			input: {
			},
			output: {
			},
			name: exports.getKey(countryProfile[COUNTRYNAME_POS])
		};
		populateOutput(countryProfile[COUNTRYNAME_POS], entry.output);
		
		// set weights
		entry.input = {
			pd: getWeight(parseInt(countryProfile[COUNTRYPD_POS]), 10),
			ua: getWeight(parseInt(countryProfile[COUNTRYUA_POS]), 10),
			ic: getWeight(parseInt(countryProfile[COUNTRYIC_POS]), 10),
			mas: getWeight(parseInt(countryProfile[COUNTRYMAS_POS]), 10),
			lto: getWeight(parseInt(countryProfile[COUNTRYLTO_POS]), 10)
		};
				
		// add to training set
		set.push(entry);
	}
}

exports.getKey = function(text) {
	return text.replace(/\s+/g, '-').toLowerCase()
};

function storeCountryNames(callback) {
	// read from a file
    var parser = parse({delimiter: ','}, function (err, data) {
    	var counter = 0;
    	var countryProfile;
        async.eachSeries(data, function (line, cb) {
          // do something with the line
    	  if (counter !== 0) {
    		  // if not headerline
    		  countryProfile = line[0].split(";");
    		  countries.push(countryProfile[COUNTRYNAME_POS]);
    		  COUNTRY_MAP[exports.getKey(countryProfile[COUNTRYNAME_POS])] = countryProfile[COUNTRYNAME_POS];
    	  } 
    	  counter++;
    	  cb();
        });
    });
    fs.createReadStream(__dirname + '/' + FILENAME).pipe(parser);
    parser.on('end', callback);
};

exports.getCountryFromKey = function(key) {
	return COUNTRY_MAP[key];
};

/** 
 * returns following format:
 * [
 * { 
 * 		output: {
 * 			england: 0,
 * 			usa: 1,
 * 			india: 0
 * 		} // defines 64 different outputs. For this to work, a map must exist with country name as value 
 * 		input: {
 * 			pd: 0.5,
 * 			ua: 0.5,
 * 			ic: 0.5,
 * 			mas: 0.5,
 * 			lto: 0.5
 * 		}
 * }
 * ]
 */
exports.getTrainingSet = function(callback) {
	storeCountryNames(function() {
		// read from a file
		var parser = parse({delimiter: ','}, function (err, data) {
			var counter = 0;
			var countryProfile;
		    for (var i = 0; i<data.length; i++) {
		    	// do something with the line
				// if not headerline
		    	if (i !== 0) {
		    		countryProfile = data[i][0].split(";");
					put(countryProfile, TRAINING_SET);
		    	}
			}
		});
		fs.createReadStream(__dirname + '/' + FILENAME).pipe(parser);
		parser.on('end', function() {
			callback(TRAINING_SET);
		});
	});
};

function binaryStringToArray(string) {
	var converted = [];
	for (var i = 0; i<string.length; i++) {
		converted.push(parseInt(string[i], 10));
	}
	return converted;
}

