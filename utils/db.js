var hofstedeFileUtil = require('./hofstedeFileUtil');

exports.DB = undefined;

function processData(trainingSet) {
	exports.DB = new loki('db.json');
	var db = exports.DB;
	var countries = db.addCollection('countries')
	var country;
	for (var i = 0; i<trainingSet.length; i++) {
		country = trainingSet[i];
		countries.insert({
			name: country.name,
			pd: country.input.pd,
			ua: country.input.ua,
			ic: country.input.ic,
			mas: country.input.mas,
			lto: country.input.lto
		});
	}
	db.saveDatabase();
}

// train if not trained
(function() {
	if (typeof exports.DB === "undefined") {	 
	    hofstedeFileUtil.getTrainingSet(function(trainingSet) {
	    	processData(trainingSet);
	    });
	}
})();

function getEmptryAnswerPoints() {
	return {
		p: {
			l: 0,
			h: 0
		},
		u: {
			l: 0,
			h: 0
		},
		i: {
			l: 0,
			h: 0
		},
		m: {
			l: 0,
			h: 0
		},
		l: {
			l: 0,
			h: 0
		}
	}
}

function getAnswerWeight(dim, answerPoints) {
	var l = answerPoints[dim].l;
	var h = answerPoints[dim].h;
	var tot = l + h;
	return (l / tot)
}

exports.getWeightedAnswers = function (req) {
	var answerMap = req.body;
	var answerPoints = getEmptryAnswerPoints();
	var charAt0, answer, response;
	for (questionKey in answerMap) {
		charAt0 = questionKey.charAt(0);
		answer = answerMap[questionKey];
		if (typeof answer === "string") {
			response = answer.charAt(7);
		} else {
			// todo
		}
		if (answerPoints[charAt0]) {
			answerPoints[charAt0][response]++;
		}	
	}
	var result = {
		pd: getAnswerWeight('p', answerPoints),
		ua: getAnswerWeight('u', answerPoints),
		mas: getAnswerWeight('m', answerPoints),
		ic: getAnswerWeight('i', answerPoints),
		lto: getAnswerWeight('l', answerPoints)
	}
	return hofstedeFileUtil.toHopfieldInput(result);
};

// implement sim method
exports.getCountry = function(weightedAnswers) {
	var db = exports.DB;
	db.find({
		'$and': [
		         {pd: { $gt : 0.25 }},
		         {pd: { $lt : 0.5 }},
		         {ua: { $gt : 0.25 }},
		         {ua: { $lt : 0.5 }},
		         {mas: { $gt : 0.25 }},
		         {mas: { $lt : 0.25 }},
		         {ic: { $gt : 0.25 }},
		         {ic: { $lt : 0.25 }},
		         {lto: { $gt : 0.25 }}
		         {lto: { $lt : 0.25 }}
		 ]
	});
};
