/**
 * Module dependencies
 */
var express = require('express'),
  routes = require('./routes'),
  api = require('./routes/api'),
  http = require('http'),
  path = require('path'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  serveStatic = require('serve-static'),
  errorHandler = require('errorhandler');

var app = module.exports = express();

/**
* Configuration
*/
var port = process.env.PORT;
port = 9999;
app.set('port', port);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(serveStatic(__dirname + '/public'))
app.use(bodyParser());
app.use(methodOverride());

if (app.get('env') === 'development') {
   app.use(errorHandler());
};

if (app.get('env') === 'production') {
  // TODO
}; 

// Routes
app.get('/', routes.index);
app.get('/partial/:name', routes.partial);
app.post('/calculate', routes.calculate);
app.get('/result/:countryKey', routes.result);

// JSON API
app.get('/api/questions', api.questions);

// redirect all others to the index (HTML5 history)
app.get('*', routes.index);

/**
* Start Server
*/
http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
